import java.util.Scanner;

public class MainClass_sphera {
	
	public static Scanner in = new Scanner(System.in);
	
	
	public static double sphereRadius() {
		System.out.println("Введите радиус сферы:");
		double radius = in.nextDouble();
		return (4 * (radius * radius * radius) * Math.PI) / 3;
	}
	

 public static void main(String[] args) {
  int temp=0, num1=0, a=0, b=0, menu=0;
  double answ=0;
        
       do {
	       System.out.println("Введите:\n 1  - если хотите работать с калькулятором; \n 2  - если хотите работать с конвертором площадей; \n 3 - если хотите расчитать площадь сферы \n 4  - для выхода и программы");
	       temp=in.nextInt();    
	       if (temp==1)
	       {
	           System.out.println("Введите:\n 1  - если хотите сложить числа; \n 2  - если хотите отнять числа;\n 3  - если хотите разделить числа; \n 4  - если хотите умножить числа");
	           menu=in.nextInt();
	           System.out.println("Введите  a:");
	           a=in.nextInt();
	           System.out.println("Введите  b:");
	           b=in.nextInt();
	            switch (menu) {
	               case 1:  answ=a+b;
	                        break;
	               case 2:  answ=a-b;
	               			break;
	               case 3:  answ=a/b;
	                        break;
	               case 4:  answ=a*b;
	                        break;
	
	                   }
	            System.out.println("Ответ:"+answ);
	       }
	       if (temp==2)
	       {
	           System.out.println("Введите:\n 1  - если хотите конвертировать  см^2 в мм^2; \n 2  - если хотите конвертировать  м^2  в см^2; \n 3  - если хотите конвертировать  м^2  в мм^2;\n 4  - если хотите конвертировать  мм^2 в см^2; \n 5  - если хотите конвертировать  мм^2 в м^2; \n 6  - если хотите конвертировать  см^2 в м^2");
	           menu=in.nextInt();
	           System.out.println("Введите число:");
	           a=in.nextInt();
	           System.out.print("Ответ: " );
	           switch (menu) {
	               case 1:  answ=a*100;
	                       System.out.println( answ + " мм^2");
	                       break;
	               case 2:  answ=a*10000;
	                        System.out.println( answ + " см^2");
	                        break;
	               case 3:  answ=a*1000000;
                   			System.out.println( answ + " см^2");
                   			break;
	               case 4:  answ=a*0.01;
	                        System.out.println( answ + " см^2");
	                        break;
	               case 5:  answ=a*0.000001;
	                        System.out.println( answ + " м^2");
	                        break;
	               case 6:  answ=a* 0.0001;
	                        System.out.println( answ + " м^2");
	                        break;    
	             }
	       }
	       if (temp==3)
	       {
	    	   double j = sphereRadius();
	    	   System.out.println("Радиус: " + j);
	       }
       }
       while (temp!=4);
       System.out.println("Спасибо за использование программы!");
       System.exit(0);
    }    
}